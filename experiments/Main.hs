module Main where

import Criterion.Types
import Criterion.Measurement
import Control.Lens
import Types.Base
import Cuckoo.HashSet
import Control.Monad.Random
import Control.Monad
import Control.Monad.ST
import Control.Monad.Writer.Lazy
import Types.HashSetAction
import Debug.Trace
import Hashing.IntHashing

data Factors = Factors {
  _input :: Either (Int, Int) [Int] -- Left (N, seed) | Right list
  , _epsilon :: Double
  , _initSize :: Int
  , _bound :: Int
  } deriving (Show)

f1 = Factors {
  _input = Left (3, 2)
  , _epsilon = 0.1
  , _initSize = 3
  , _bound = 70
  }

f2 = Factors {
  _input = Left (30, 2)
  , _epsilon = 0.1
  , _initSize = 3
  , _bound = 700
  }

f3 = Factors {
  _input = Right [488, 104, 692]
  , _epsilon = 0.1
  , _initSize = 12
  , _bound = 700
  }


f4 = Factors {
  _input = Left (1000, 1)
  , _epsilon = 0.1
  , _initSize = 12
  , _bound = 7000
  }


t = runExperiment >=> print

h p r a b x = [(a*x + b `mod` p)`mod`r]

-- |
-- p: prime
-- r: size of the table. r < p
-- x: integer in [0..p-1]
-- y: integer in [0..p-1]
g p r x y z = [ (a, b) | a <- [0..p-1], b <- [0..p-1]
                       , h p r a b x /= h p r a b y ||
                         h p r a b x /= h p r a b z]

-- 488 104 692

gg = let r = g 2003 12 488 104 692
     in if null r
        then "RIP"
        else "YAY" ++ show (head r)


getNs :: Factors -> [Int]
getNs Factors{_input = Left (_n, _seed),_bound =_bound} =
  evalRand (take _n <$> getRandomRs (0, _bound)) (mkStdGen _seed)
getNs Factors{_input = Right ns} = ns

{-
infinite rehash
when incCard before startBounce
Factors {_n = 3, _epsilon = 0.1, _maxBounces = 50, _initSize = 3, _bound = 70, _seed = 2}

when incCard before or after (always) startBounce
Factors {_n = 30, _epsilon = 0.1, _maxBounces = 50, _initSize = 3, _bound = 700, _seed = 2}
-}
runExperiment :: Factors -> IO Int
runExperiment fac@Factors{..} =
  fmap fst . runWriterT $ do
  traceM (show ns)
  hs <-  newHashSet (_bound + 1) _initSize _epsilon
  forM_ ns (insert hs)
  lift (size hs)
    where
      ns = getNs fac



main :: IO ()
main = do
  putStrLn "Main"
  runExperiment f4 >>= print
