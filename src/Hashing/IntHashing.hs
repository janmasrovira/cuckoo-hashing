-- | Family of hash functions /h_a,b(x) = ((ax + b) mod p)/

module Hashing.IntHashing (
  newUniversalHashFamily
  , getHashFun
  , bounds
  , nextPrime
  ) where


import Control.Monad.Random
import Data.Numbers.Primes
import Debug.Trace
import Types.Hashing
import Data.List

-- | Creates a new hash family.
newUniversalHashFamily ::
  Int -- ^ exclusive upper bound
  -> Int
  -> UniversalHashFamily
newUniversalHashFamily u = UHF w (nextPrime (2^w))
  where
    w = ceiling (logBase 2 (fromIntegral u :: Double))

-- | Picks u.a.r a hash function from the family.
getHashFun :: MonadRandom m => UniversalHashFamily -> m (HashFunction Int)
getHashFun (UHF _ p k) = do
  as <- take k <$> getRandomRs (0, p - 1)
  --traceM ("getHashFun " ++ show (as, p))
  return $ \x -> fromIntegral $ evalPoly as x `mod` toInteger p

-- | Returns the smallest prime such that is greater than the parameter.
nextPrime :: Int -> Int
nextPrime x = head $ filter (x<) primes

-- | Inclusive domain bounds of the hash family.
bounds :: UniversalHashFamily -> (Int, Int)
bounds (UHF w _ _) = (0, 2^w - 1)

evalPoly :: (Num a, Integral a) => [a] -> a -> Integer
evalPoly coeffs' x' = sum [ c*x^l | (c, l) <- zip [0..] coeffs]
  where sum = foldl' (+) 0
        coeffs = map toInteger coeffs'
        x = toInteger x'
