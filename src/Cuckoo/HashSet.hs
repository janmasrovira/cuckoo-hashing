-- In order to check that the examples are correct, run
-- stack exec doctest -- -XMultiWayIf src/CuckooHashSet.hs
-- |
-- This module exports the functions you should use from 'Cuckoo.HashSetInternal'
-- Example:
--
-- >>> 5 + 5
-- 10

module Cuckoo.HashSet (
  newHashSet
  , contains
  , delete
  , insert
  , size
  ) where

import           Cuckoo.HashSetInternal
