-- | Functions used internally

module Cuckoo.HashSetInternal where

import           ClassyPrelude (whenM, unlessM)
import           Control.Lens                ((^.))
import           Control.Monad
import           Control.Monad.Loops
import           Control.Monad.Primitive
import           Control.Monad.Random
import           Control.Monad.Writer.Lazy
import qualified Cuckoo.FrozenHashSet        as F
import           Data.Ix                     (inRange)
import           Data.Primitive.MutVar
import           Data.Tuple
import qualified Data.Vector.Unboxed.Mutable as V
import           Debug.Trace
import           Hashing.IntHashing
import           Types.Base
import           Types.HashSet
import           Types.HashSetAction


showPos :: (Int, Int) -> [Char]
showPos (ti, ix) = "t" ++ show ti ++ "[" ++ show ix ++ "]"

-- | Creates an empty Cuckoo Hash Set. The initial tables have size m.
--
-- Allowed values in range of @[0..u - 1]@.
--
-- When performing an insertion, does a maximum of maxIt iterations.
newHashSet :: (PrimRand m)
              => Int -- ^ upper bound (exclusive).
              -> Int -- ^ initial table size.
              -> Double -- ^ epsilon > 0
              -> WriterT [HashSetAction] m (HashSet m)
newHashSet u m eps = do
  [h1, h2] <- replicateM 2 (getHashFun fam) >>= mapM newMutVar
  [t1, t2] <- replicateM 2 (V.replicate m empty) >>= mapM newMutVar
  c <- newMutVar 0
  let hs = HashSet {
        _hashFamily = fam
        , _h1 = h1
        , _h2 = h2
        , _t1 = t1
        , _t2 = t2
        , _card = c
        , _eps = eps
        }
  tellSnapshot hs "Initial state"
  return hs
  where
    fam = newUniversalHashFamily u k
    k = 10

-- | Tests whether an element is in the set.
contains :: PrimMonad m => HashSet m -> Int -> WriterT [HashSetAction] m Bool
contains hs x = do
  (i1, i2) <- lift (index hs x)
  orM [(== x) <$> (readMutVar (hs ^. t1) >>= flip V.read i1)
      , (== x) <$> (readMutVar (hs ^. t2)  >>= flip V.read i2)]

-- | Deletes an element if it is in the set.
delete :: PrimRand m => HashSet m -> Int -> WriterT [HashSetAction] m ()
delete hs x = sequence_ [deleteFrom 1, deleteFrom 2]
  where
    deleteFrom ti = do
      t <- lift (readTable ti hs)
      i <- lift (indexi ti hs x)
      whenM ((== x) <$> V.read t i)
        (tellDelete hs x >> V.write t i empty >> decrCard hs)

-- | Inserts an element to the set.
insert :: PrimRand m => HashSet m -> Int -> WriterT [HashSetAction] m ()
insert hs x
  | inRange bs x = unsafeInsert hs x
  | otherwise = error $ "Element " ++ show x ++ " out of bounds " ++ show bs
  where bs = bounds (hs ^. hashFamily)


-- | Returns the number of elements in the set.
--
-- /O(1)/
size :: PrimMonad m => HashSet m -> m Int
size hs = readMutVar (hs ^. card)


-- | Index function for both tables.
index :: PrimMonad m => HashSet m -> Int -> m (Ix, Ix)
index hs x = liftM2 (,) (index1 hs x) (index2 hs x)


indexi :: PrimMonad m => Int -> HashSet m -> Int -> m Ix
indexi 1 = index1
indexi 2 = index2
indexi x = error $ "only two tables, wrong id: " ++ show x

-- | Index function for table 1
index1 :: PrimMonad m => HashSet m -> Int -> m Ix
index1 hs x
  | x == empty = errEmptyIndex
  | otherwise = do
  rh1 <- readMutVar (hs ^. h1)
  rt1 <- readMutVar (hs ^. t1)
  let i1 = rh1 x `mod` V.length rt1
  --traceM ("ix1 " ++ show x ++ " " ++ show i1)
  return i1

errEmptyIndex = error "cannot index empty element"

-- | Index function for table 2
index2 :: PrimMonad m => HashSet m -> Int -> m Ix
index2 hs x
  | x == empty = errEmptyIndex
  | otherwise = do
  rh2 <- readMutVar (hs ^. h2)
  rt2 <- readMutVar (hs ^. t2)
  let i2 = rh2 x `mod` V.length rt2
  --traceM ("ix2 " ++ show x ++ " " ++ show i2)
  return i2

-- | changes the hash functions and moves the elements accordingly.
rehash :: PrimRand m => HashSet m -> WriterT [HashSetAction] m ()
rehash hs = do
  l <- lift (loadFactor hs)
  traceM ("rehashing, load factor: " ++ show l)
  [h1', h2'] <- replicateM 2 (getHashFun (hs ^. hashFamily))
  forM_ [(h1, h1'), (h2, h2')] $ \(hi, hi') -> writeMutVar (hs ^. hi) hi'
  tellRehash hs
  forM_ [1, 2] (rehashTable hs)
  traceM "rehashing ended"


loadFactor :: PrimMonad m => HashSet m -> m Double
loadFactor hs = do
  c <- fromIntegral <$> size hs
  r <- fromIntegral <$> tableSize hs
  return (c/(2*r))


tableSize :: PrimMonad f => HashSet f -> f Int
tableSize hs = V.length <$> readTable 1 hs


-- | It moves all the elements in a table to its new position.
-- Should only be used internally during the rehashing phase.
rehashTable :: PrimRand m => HashSet m -> Int -> WriterT [HashSetAction] m ()
rehashTable hs ti = do
  t <- lift (readTable ti hs)
  forM_ [0 .. V.length t - 1] (rehashPos hs ti)

-- | It moves an element to its new position.
-- Should only be used internally during the rehashing phase.
rehashPos :: PrimRand m
             => HashSet m -> Int -> Int -> WriterT [HashSetAction] m ()
rehashPos hs ti i = do
  t <- lift (readTable ti hs)
  x <- V.read t i
  when (x /= empty) $ do
    newIx <- lift (indexi ti hs x)
    when (newIx /= i) $ do
      tellSnapshot hs $ "Rehashing " ++ show x
        ++ "\nfrom " ++ showPos (ti, i)
        ++ "\nto " ++ showPos (ti, newIx)
      V.write t i empty
      startBounce hs x
      tellSnapshot hs ""


-- | __Precondition:__ The element is guaranteed not to be in the set.
--
-- __IMPORTANT__: It does not increase the cardinality of the set.
startBounce :: PrimRand m => HashSet m -> Int -> WriterT [HashSetAction] m ()
startBounce hs x = do
  let startT = 1 -- to reduce query time (insertions only slightly slower)
  bs <- lift (maxBounces hs)
  bounce bs hs startT x


maxBounces :: PrimMonad m => HashSet m -> m Int
maxBounces hs = do
  r <- fromIntegral <$> tableSize hs
  return $ ceiling (3*logBase (1 + (hs ^. eps)) r)

readTable :: PrimMonad m => Int -> HashSet m -> m (MVector m)
readTable 1 hs = readMutVar (hs ^. t1)
readTable 2 hs = readMutVar (hs ^. t2)
readTable x _ = error $ "table index must be 1 or 2, not " ++ show x

writeTables :: PrimMonad m => HashSet m -> (MVector m, MVector m) -> m ()
writeTables hs (t1', t2') = writeMutVar (hs ^. t1) t1' >> writeMutVar (hs ^. t2) t2'

otherTable :: Int -> Int
otherTable 1 = 2
otherTable 2 = 1
otherTable x = error $ "not a table id: " ++ show x

bounce :: PrimRand m
          => Int -- ^ Remaining bounces until rehashing.
          -> HashSet m -- ^ 'HashSet' being bounced.
          -> Int -- ^ Table id (1|2)
          -> Int -- ^ Bounced key (to be inserted)
          -> WriterT [HashSetAction] m ()
bounce 0 hs _ x = do
  rehash hs
  tellSnapshot hs "Rehash completed"
  startBounce hs x
bounce n hs tix x = do
  t <- lift (readTable tix hs)
  ix <- lift (indexi tix hs x)
  old <- V.read t ix
  --traceM $ "bounce, old = " ++ show old
  if
    | old == empty -> V.write t ix x
    | otherwise -> do
        -- traceM ("conflict at table " ++ show tix ++ ", ix " ++ show ix ++ ", old " ++ show old
        --        ++ ", new " ++ show x ++ ", r = " ++ show (V.length t))
        oldPossibleIxs <- lift (index hs old)
        size <- lift $ size hs
        -- traceM ("indexs of " ++ show old ++ ": " ++ show oldPossibleIxs ++
        --         ", r = " ++ show (V.length t) ++ ", card = " ++ show size)
        let toT = otherTable tix
        toIx <- lift (indexi toT hs old)
        tellBounce hs (tix, ix) (toT, toIx) (n - 1)
        V.write t ix x
        bounce (n - 1) hs toT old


-- | Returns the elements in the set in not specific order.
elems :: PrimMonad m => HashSet m -> m [Int]
elems hs = do
  t1 <- readTable 1 hs
  t2 <- readTable 2 hs
  xt1 <- forM [0..V.length t1 - 1] (V.read t1)
  xt2 <- forM [0..V.length t2 - 1] (V.read t2)
  return (filter (/=empty) (xt1 ++ xt2))

-- | Grows the size of both tables and rehashes.
grow :: PrimRand m => HashSet m -> WriterT [HashSetAction] m ()
grow hs = do
  rt1 <- lift (readTable 1 hs)
  rt2 <- lift (readTable 2 hs)
  let r = V.length rt1
  tellSnapshot hs "Growing tables"
  traceM $ "growing to " ++ show (r*growFactor)
  t1' <- V.grow rt1 (r * (growFactor - 1))
  t2' <- V.grow rt2 (r * (growFactor - 1))
  forM_ [r .. V.length t1' - 1] (\ix -> V.write t1' ix empty)
  forM_ [r .. V.length t2' - 1] (\ix -> V.write t2' ix empty)
  lift (writeTables hs (t1', t2'))
  rehash hs
    where
      growFactor :: Int
      growFactor = 2


-- | Takes a snapshot of the current state and passes it to the writer.
tellSnapshot :: PrimMonad m => HashSet m -> String -> WriterT [HashSetAction] m ()
tellSnapshot hs title = lift (F.freeze hs) >>= tell . pure . Snapshot title

-- | Tells a deletion.
tellDelete :: PrimMonad m => HashSet m -> Int -> WriterT [HashSetAction] m ()
tellDelete hs d = lift (F.freeze hs) >>= tell . pure . Delete d

-- | Tells a pre-insertion.
tellPreInsert :: PrimMonad m => HashSet m -> Int -> Int -> WriterT [HashSetAction] m ()
tellPreInsert hs x t
  | t == 1 || t == 2 = do
      ix <- lift (indexi t hs x)
      lift (F.freeze hs) >>= tell . pure . PreInsert x t ix
  | otherwise = error "t must be 1 or 2"


tellRehash :: PrimMonad m => HashSet m -> WriterT [HashSetAction] m ()
tellRehash hs = lift (F.freeze hs) >>= tell . pure . Rehash

-- | Tells a bounce
tellBounce :: PrimMonad m => HashSet m -> (Int, Ix) -> (Int, Ix) -> Int -> WriterT [HashSetAction] m ()
tellBounce hs orig dest rem =
  lift (F.freeze hs) >>= tell . pure . Bounce orig dest rem

-- | Increases the cardinality of the set and grows the tables when necessary.
incCard :: PrimRand m => HashSet m -> WriterT [HashSetAction] m ()
incCard hs = do
  modifyMutVar' (hs ^. card) (+1)
  maybeGrow hs

-- | Grows the size of the tables if the load factor is too high.
maybeGrow  :: PrimRand m => HashSet m -> WriterT [HashSetAction] m ()
maybeGrow hs = do
  n <- fromIntegral <$> lift (size hs)
  r <- fromIntegral <$> lift (tableSize hs)
  --traceM $ show (c, m)
  when (r <= (1 + (hs ^. eps))*n) (grow hs)

decrCard :: PrimRand m => HashSet m -> WriterT [HashSetAction] m ()
decrCard hs = modifyMutVar' (hs ^. card) pred


-- | Inserts an element to the set without checking bounds.
unsafeInsert :: PrimRand m => HashSet m -> Int -> WriterT [HashSetAction] m ()
unsafeInsert hs x = unlessM (contains hs x) $ do
  incCard hs -- TODO: place this before or after startBounce ?
  tellPreInsert hs x 1
  startBounce hs x
  tellSnapshot hs (show x ++ " inserted")
