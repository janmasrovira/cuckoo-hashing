module Cuckoo.FrozenHashSet (
  freeze
  , index
  , index1
  , index2
  , tablei
  , table1
  , table2
  , keyAt
  ) where

import           Control.Lens            ((^.))
import           Control.Monad.Primitive
import           Data.Primitive.MutVar
import qualified Data.Vector.Unboxed     as U
import           Other.RandPrimMonad     ()
import           Types.Base
import           Types.FrozenHashSet
import qualified Types.HashSet           as H
import           Types.Hashing

-- | Creates an immutable copy of a 'HashSet'
freeze :: PrimMonad m => H.HashSet m -> m FrozenHashSet
freeze hs = do
  _h1 <- readMutVar (hs ^. H.h1)
  _h2 <- readMutVar (hs ^. H.h2)
  _t1 <- readMutVar (hs ^. H.t1) >>= U.freeze
  _t2 <- readMutVar (hs ^. H.t2) >>= U.freeze
  _card <- readMutVar (hs ^. H.card)
  return FrozenHashSet {
    _h1 = _h1
    , _h2 = _h2
    , _t1 = _t1
    , _t2 = _t2
    , _card = _card
    }


index :: FrozenHashSet -> Int -> (Ix, Ix)
index hs x = (index1 hs x, index2 hs x)

index1 :: FrozenHashSet -> Int -> Ix
index1 hs x = (hs ^. h1) x `mod` U.length (hs ^. t1)

index2 :: FrozenHashSet -> Int -> Ix
index2 hs x = (hs ^. h2) x `mod` U.length (hs ^. t2)

tablei :: Int -> FrozenHashSet -> U.Vector Int
tablei 1 = table1
tablei 2 = table2

table1 :: FrozenHashSet -> U.Vector Int
table1 = ( ^. t1)

table2 :: FrozenHashSet -> U.Vector Int
table2 = ( ^. t2)

keyAt :: FrozenHashSet -> Int -> Int -> Int
keyAt hs t = (tablei t hs U.!)
