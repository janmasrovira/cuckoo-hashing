module CuckooMain (cuckooMain) where

import Control.Monad
import Control.Monad.Primitive
import Control.Monad.Random
import Control.Monad.ST
import Control.Monad.Writer.Lazy
import Cuckoo.HashSet
import Diagrams.Backend.Cairo
import Diagrams.CuckooHashSet
import Diagrams.Generic
import Diagrams.Prelude
import Other.RandPrimMonad       ()
import Types.Base
import System.Directory
import ClassyPrelude (whenM)
import System.FilePath ((</>), (<.>))
import Types.HashSetAction
import Types.HashSet
import Hashing.IntHashing

data Factors = Factors {
  _input :: Either (Int, Int) [Int] -- Left (N, seed) | Right list
  , _epsilon :: Double
  , _initSize :: Int
  , _bound :: Int
  } deriving (Show)

f1 = Factors {
  _input = Left (6, 3)
  , _epsilon = 0.1
  , _initSize = 3
  , _bound = 70
  }



runExperiment :: Factors -> [HashSetAction]
runExperiment fac@Factors{..} =
  runST $ flip evalRandT (mkStdGen 1) $ fmap snd . runWriterT $ do
  hs <-  newHashSet (_bound + 1) _initSize _epsilon
  forM_ ns (insert hs)
    where
      ns = getNs fac
      getNs :: Factors -> [Int]
      getNs Factors{_input = Left (_n, _seed),_bound =_bound} =
        evalRand (take _n <$> getRandomRs (0, _bound)) (mkStdGen _seed)
      getNs Factors{_input = Right ns} = ns


y :: [HashSetAction]
y = runST $ flip evalRandT (mkStdGen 1) $ execWriterT $ do
  hs <- newHashSet u m eps
  ns <- take 20 <$> getRandomRs (bounds (hs ^. hashFamily))
  forM_ ns (insert hs)
  --forM_ [1,2] (delete hs)
  --forM_ [1,2,5] (insert hs)
  where u = 20
        m = 5
        maxIt = 15
        n = 20
        eps = 0.1
        --ns = [39,79,29]


cuckooMain :: IO ()
cuckooMain = main

mkAndClearFolder :: FilePath -> IO ()
mkAndClearFolder f = do
  whenM (doesDirectoryExist f) (removeDirectoryRecursive f)
  createDirectoryIfMissing True f

diagFolder :: FilePath
diagFolder = "diagrams"

main :: IO ()
main = do
  mkAndClearFolder diagFolder
  renderManyCairo' file dias
  where
    dias = map drawCuckooState (runExperiment f1)
    file i = diagFolder </> "state" ++ show i <.> ".png"
