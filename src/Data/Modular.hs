module Data.Modular (
  Mod
  , eval
  ) where

data Mod a = Term a
           | Sum (Mod a) (Mod a)
           | Sub (Mod a) (Mod a)
           | Mul (Mod a) (Mod a)
           | Sig (Mod a)
           deriving (Show, Eq)

instance (Num a) => Num (Mod a) where
  a + b = Sum a b
  a * b = Mul a b
  a - b = Sub a b
  abs a = a
  signum = Sig
  fromInteger a = Term (fromInteger a)

eval :: (Num a, Integral a) => a -> Mod a -> a
eval m = eval'
  where
    modm = (`mod`m)
    evalm = modm . eval'
    eval' (Sum a b) = evalm a + evalm b
    eval' (Sub a b) = evalm a - evalm b
    eval' (Mul a b) = evalm a * evalm b
    eval' (Sig a) = signum (evalm a)
    eval' (Term a) = modm a
