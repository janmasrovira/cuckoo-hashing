{-# LANGUAGE FlexibleContexts #-}

module Diagrams.CuckooHashSet (
  drawCuckooState
  ) where

import           Control.Monad.Primitive
import           Cuckoo.FrozenHashSet
import           Data.Colour.RGBSpace
import qualified Data.Vector.Unboxed     as U
import qualified Debug.Trace             as T
import           Diagrams.Backend.Cairo
import           Diagrams.Generic
import           Diagrams.Prelude        hiding (index, text)
import qualified Diagrams.Prelude        as D
import           Types.Base
import           Types.FrozenHashSet
import           Types.HashSetAction

text x = D.text x # fontSizeL 0.5 # font "monospace"

-- | Colour for empty cells.
emptyC :: Colour Double
emptyC = gainsboro

-- | Colour for keys in their nest.
correctC :: Colour Double
correctC = lightgreen

-- | Colour for keys in an incorrect nest.
incorrectC :: Colour Double
incorrectC = lightcoral

-- | Colour for a just emptied nest.
deleteC :: Colour Double
deleteC = darkgrey

-- | Insert target is empty
targetEmptyC :: Colour Double
targetEmptyC = cornflowerblue

-- | Insert target is not empty
targetConflictC :: Colour Double
targetConflictC = orange

-- | Size of a cell side.
cellSide :: Double
cellSide = 3

-- | Separation between the two tables.
tsep :: Double
tsep = 3*cellSide

-- | Draws in a 'Diagram' a 'HashSetAction'.
drawCuckooState :: HashSetAction -> Diagram B
drawCuckooState (Snapshot title hs) = hsep (tsep/2) [d1, text title, d2]
  where
    d1 = drawTable (checkContents (index1 hs)) (hs ^. t1)
    d2 = drawTable (checkContents (index2 hs)) (hs ^. t2)
drawCuckooState (Delete d hs) =
  hsep tsep [d1, d2]
  where
    d1 = drawTable (delContents (index1 hs) d) (hs ^. t1)
    d2 = drawTable (delContents (index2 hs) d) (hs ^. t2)
    delContents :: (Int -> Ix) -> Int -> Ix -> Int -> Diagram B
    delContents h d ix x
      | x == d = cell black deleteC (text (show d) # opacity 0.25)
      | otherwise = checkContents h ix x
drawCuckooState (PreInsert x toT toIx hs) =
  hsep (tsep/2) [d1, text title, d2]
  where
    conflict = tablei toT hs U.! toIx /= empty
    title = ("Insert " ++ show x) ++
            (if conflict
             then "\nConflict!" else "")
    d1 = drawTable (contents (index1 hs) 1) (hs ^. t1)
    d2 = drawTable (contents (index2 hs) 2) (hs ^. t2)
    contents :: (Int -> Ix) -> Int -> Ix -> Int -> Diagram B
    contents h t curIx x
      | t == toT && curIx == toIx =
          let (dx, bgc)
                | x == empty = (mempty, targetEmptyC)
                | otherwise = (text (show x), targetConflictC)
          in cell black bgc dx
      | otherwise = checkContents h curIx x
drawCuckooState (Bounce (fromT, fromIx) (toT, toIx) rem hs) =
  con "from" "to" $ hsep (tsep/2) [d1, text title, d2]
  where
    title
      | rem == 0 = "Out of bounces!"
      | otherwise = "Bounce (" ++ show (keyAt hs fromT fromIx)
                    ++ "), \nRemaining: " ++ show rem
    d1 = drawTable (contents (index1 hs) 1) (hs ^. t1)
    d2 = drawTable (contents (index2 hs) 2) (hs ^. t2)
    contents :: (Int -> Ix) -> Int -> Ix -> Int -> Diagram B
    contents h t curIx x
      | t == fromT && fromIx == curIx =
          cell black deleteC (text (show x) # opacity 0.25 # named "from")
      | t == toT && toIx == curIx =
          let (dx, bgc)
                | x == empty = (mempty, targetEmptyC)
                | otherwise = (text (show x), targetConflictC)
          in cell black bgc (dx # named "to")
      | otherwise = checkContents h curIx x
drawCuckooState (Rehash hs) = hsep (tsep/2) [d1, text ("Rehash"), d2]
  where
    d1 = drawTable (checkContents (index1 hs)) (hs ^. t1)
    d2 = drawTable (checkContents (index2 hs)) (hs ^. t2)


-- | Checks if the element is in an incorrect nest.
checkContents :: (Int -> Ix) -> Ix -> Int -> Diagram B
checkContents h ix x
      | x == empty = cell black emptyC mempty
      | h x == ix = cell black correctC (text (show x))
      | otherwise = T.trace ("item " ++ show x ++ " at pos " ++ show ix ++ ", it should be at "
                            ++ show (h x)) $
                    cell black incorrectC (text (show x))


-- | Draws the elements in a table vertically.
drawTable :: (Ix -> Int -> Diagram B) -> U.Vector Int -> Diagram B
drawTable cont = drawCol cont . U.toList


cell :: Colour Double -> Colour Double -> Diagram B -> Diagram B
cell lcc bgc = (<> rect cellSide cellSide # bg bgc # lw thick # lc lcc)


drawCol :: (Ix -> Int -> Diagram B) -> [Int] -> Diagram B
drawCol contents xs = vcat [ contents ix x | (ix, x) <- zip [0..] xs]



con :: String -> String -> Diagram B -> Diagram B
con a b = connect' (with & arrowShaft .~ shaft3
                    & arrowHead .~ tri & headLength .~ normal
                    & gaps .~ normal
                    & headStyle %~ fc firebrick . opacity 1
                    & shaftStyle %~ lw veryThick . lc black . opacity 0.6 . dashingN [0.05,0.02] 0) a b
  where shaft3 = arc xDir ((1::Double)/6 @@ turn)

----------------
-- PLAYGROUND --
----------------

tmp = "tmp.png"


r = renderCairo' tmp d
  where
    d :: Diagram B
    d = con "a" "b" $ hsep (2 :: Double) ds
    ds :: [Diagram B]
    ds = [circle 1 # fc red # named "a", circle 1 # fc green # named "b"]
