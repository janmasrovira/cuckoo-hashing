module Diagrams.Generic (
  renderCairo'
  , renderManyCairo'
  ) where

import Diagrams.Prelude
import Diagrams.Backend.Cairo

c :: Diagram B
c = text (show 123) <> circle 1 # bg red

renderCairo' :: FilePath -> Diagram B -> IO ()
renderCairo' = flip renderCairo _sized

renderManyCairo' :: (Int -> FilePath) -> [Diagram B] -> IO ()
renderManyCairo' f ds = sequence_ [renderCairo' (f i) d | (i, d) <- zip [0..] ds]

_width :: Double
_width = k*100

k :: Double
k = 9

_sized :: SizeSpec V2 Double
_sized = mkSizeSpec2D (Just _width) Nothing
