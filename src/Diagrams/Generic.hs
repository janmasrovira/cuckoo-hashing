module Diagrams.Generic (
  renderCairo'
  , renderManyCairo'
  ) where

import Diagrams.Prelude
import Diagrams.Backend.Rasterific

c :: Diagram B
c = text (show 123) <> circle 1 # bg red

renderCairo' :: FilePath -> Diagram B -> IO ()
renderCairo' file = renderPdf _width _height file _sized

renderManyCairo' :: (Int -> FilePath) -> [Diagram B] -> IO ()
renderManyCairo' f ds = sequence_ [renderCairo' (f i) d | (i, d) <- zip [0..] ds]

_width :: Int
_width = k*100

_height :: Int
_height = k*100

k :: Int
k = 9

_sized :: SizeSpec V2 Double
_sized = mkSizeSpec2D (Just (fromIntegral _width)) Nothing
