{-# LANGUAGE TypeFamilies #-}

module Other.RandPrimMonad where

import           Control.Monad.Primitive
import           Control.Monad.Random
import           Control.Monad.Trans


-- https://gist.github.com/sdroege/6209e97b4dfc9791033d
-- instance PrimMonad m => PrimMonad (RandT g m) where
--     type PrimState (RandT g m) = PrimState m
--     primitive = lift . primitive
