{-# LANGUAGE ConstraintKinds #-}
module Types.Base (
  PrimRand
  , Dup
  , MVector
  , Ix
  , empty
  ) where

import           Control.Monad.Primitive
import           Control.Monad.Random
import qualified Data.Vector.Unboxed.Mutable as V


-- | Index.
type Ix = Int


-- | A mutable vector of 'Int' \'s.
type MVector m = V.MVector (PrimState m) Int

-- | Pair of the same type.
type Dup a = (a, a)

-- | Intersection of 'MonadRandom' and 'PrimMonad'.
type PrimRand m = (MonadRandom m, PrimMonad m)

-- | Empty value
empty :: Int
empty = -1
