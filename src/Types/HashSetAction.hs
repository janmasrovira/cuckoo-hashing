module Types.HashSetAction (
  HashSetAction(..)
  ) where

import Types.FrozenHashSet
import Types.Base

-- | Actions of a 'HashSet'.
data HashSetAction =
  Snapshot String FrozenHashSet -- ^ A snapshot with a title.
  | Delete Int FrozenHashSet -- ^ Deletion of a key.
  | PreInsert Int Int Ix FrozenHashSet -- ^ Before inserting a key. Key (1|2) Index.
  | Bounce (Int, Ix) (Int, Ix) Int FrozenHashSet -- ^ Movement of a key. origin@(table, ix) @dest(table,ix) remainingBounces
  | Rehash FrozenHashSet -- ^ Rehash
