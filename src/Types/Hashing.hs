module Types.Hashing (
  HashFunction
  , UniversalHashFamily (UHF)
  ) where

-- | A function mapping something to an 'Int'.
type HashFunction a = a -> Int


-- | A universal family of hash functions.
data UniversalHashFamily
     -- | Integer hashing by Carter and Wegman. The arguments are the length of the words /w/ and the prime number /p/ used to compute the modulo, the last argument, /k/, means that the family is (2,/k/)-universal.
     = UHF Int Int Int
     deriving (Show)
