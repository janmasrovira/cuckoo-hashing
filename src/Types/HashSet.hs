{-# LANGUAGE TemplateHaskell #-}

module Types.HashSet (
  h1
  , h2
  , t1
  , t2
  , hashFamily
  , card
  , eps
  , HashSet(..)
   ) where

import Types.Base
import Control.Monad.Primitive
import Data.Primitive.MutVar
import Control.Lens.TH
import Types.Hashing

-- | An implementation of a HashSet using Cuckoo Hashing with two tables.
data HashSet m =
  HashSet {
  _hashFamily :: UniversalHashFamily -- | Universal hash family.
  , _h1 :: MutVar (PrimState m) (HashFunction Int) -- ^ Hash function for the first table.
  , _h2 :: MutVar (PrimState m) (HashFunction Int) -- ^ Hash function for the second table.
  , _t1 :: MutVar (PrimState m) (MVector m) -- ^ First table.
  , _t2 :: MutVar (PrimState m) (MVector m) -- ^ Second table.
  , _card :: MutVar (PrimState m) Int -- ^ Maintained cardinality of the set
  , _eps :: Double -- ^ Tables grow when r < (1 + eps)*size
  }
makeLenses ''HashSet
