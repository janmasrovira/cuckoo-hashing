{-# LANGUAGE TemplateHaskell #-}

module Types.FrozenHashSet (
  h1
  , h2
  , t1
  , t2
  , card
  , FrozenHashSet(..)
  ) where

import           Control.Lens        ((^.))
import           Control.Lens.TH
import           Data.Vector.Unboxed (Vector)
import qualified Data.Vector.Unboxed as U
import           Types.Base
import           Types.Hashing

-- | Immutable 'HashSet'.
data FrozenHashSet =
  FrozenHashSet {
  _h1 :: HashFunction Int
  , _h2 :: HashFunction Int
  , _t1 :: Vector Int
  , _t2 :: Vector Int
  , _card :: Int
  }
makeLenses ''FrozenHashSet
